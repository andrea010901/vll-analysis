redirector = "root://cmsxrootd.fnal.gov//"

data = {
    "Data_SingleEle_runF_2016": [
        redirector+"/store/data/Run2016F/SingleElectron/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/130000/31BADB98-BCD8-4B40-9825-7709B08299BD.root",
        redirector+"/store/data/Run2016F/SingleElectron/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/130000/3E658EF7-F6B2-8542-AF10-304B350655F1.root",
        redirector+"/store/data/Run2016F/SingleElectron/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/130000/0F8521B0-5770-9244-915E-F643EDAA32D2.root",
        redirector+"/store/data/Run2016F/SingleElectron/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/130000/A087F674-68BF-AF45-8E15-CCC6028B2576.root",
        redirector+"/store/data/Run2016F/SingleElectron/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/130000/5FC6D4C8-7C25-0D4B-9802-C89AA5F298AA.root"
    ],
    "Data_SingleMu_runF_2016": [
        redirector+"/store/data/Run2016F/SingleMuon/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/130000/FF9B5EE5-4C8E-3F41-B608-AC8DD47E51F2.root",
        redirector+"/store/data/Run2016F/SingleMuon/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/130000/A6CA373A-583D-BD42-9556-0351CF148FDC.root",
        redirector+"/store/data/Run2016F/SingleMuon/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/130000/7F5791FE-262B-EA45-BE19-04E4F21FEE1E.root",
        redirector+"/store/data/Run2016F/SingleMuon/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/70000/0B54974C-D6CA-0748-9ED9-03BB3DDD82C8.root",
        redirector+"/store/data/Run2016F/SingleMuon/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/70000/15A7AA88-B1A1-384F-8AA5-13031C1A5E87.root"
    ]
}
