import time

import coffea.processor as processor
from coffea.analysis_tools import PackedSelection, Weights
from coffea.nanoevents import NanoAODSchema, NanoEventsFactory
from coffea.nanoevents.methods import nanoaod

import hist

NanoAODSchema.warn_missing_crossrefs = False

import pickle
import re

import awkward as ak
import numpy as np

from .scalefactors.puScalefactors import *
from .scalefactors.jerjesCorrections import *

from .scalefactors import (
    ceval2016pre,
    ceval2016post,
    ceval2017,
    ceval2018,
    ele_id_err,
    ele_id_sf,
    ele_reco_err,
    ele_reco_sf,
#    mu_id_err,
#    mu_id_sf,
#    mu_iso_err,
#    mu_iso_sf,
#    mu_trig_err,
#    mu_trig_sf,
)

def btagSF(j, syst="central",era="2018"):
    # until correctionlib handles jagged data natively we have to flatten and unflatten
    j, nj = ak.flatten(j), ak.num(j)
    if(era=="2018"):
        sf = ceval2018["deepJet_shape"].evaluate(syst,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(j.pt),np.array((j.btagDeepFlavB)))
    elif(era=="2017"):
        sf = ceval2017["deepJet_shape"].evaluate(syst,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(j.pt),np.array((j.btagDeepFlavB)))
    elif(era=="2016post"):
        sf = ceval2016post["deepJet_shape"].evaluate(syst,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(j.pt),np.array((j.btagDeepFlavB)))
    elif(era=="2016pre"):
        sf = ceval2016pre["deepJet_shape"].evaluate(syst,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(j.pt),np.array((j.btagDeepFlavB)))
    else:
        raise Exception(f"Error: Unknown era \"{era}\".")

    return ak.unflatten(sf, nj)

def selectMuons(events):

    muonSelectTight = (
        (events.Muon.pt > 10.) & (abs(events.Muon.eta) < 2.4)
        & ((events.Muon.isPFcand) & (events.Muon.isTracker | events.Muon.isGlobal))
        & (events.Muon.pfRelIso04_all < 0.25) 
        & (events.Muon.tightId)
    )  

    muonSelectLoose = (
        (events.Muon.pt > 10) & (abs(events.Muon.eta) < 2.4)
        & ((events.Muon.isPFcand) & (events.Muon.isTracker | events.Muon.isGlobal))
        & (events.Muon.pfRelIso04_all < 0.25) 
        & (events.Muon.looseId) & np.invert(muonSelectTight)
    )

    return events.Muon[muonSelectTight], events.Muon[muonSelectLoose]


def selectElectrons(events):
    eleEtaGap = (abs(events.Electron.eta) < 1.4442) | (abs(events.Electron.eta) > 1.566)
    elePassDXY = (abs(events.Electron.eta) < 1.479) & (abs(events.Electron.dxy) < 0.05) | (abs(events.Electron.eta) > 1.479) & (abs(events.Electron.dxy) < 0.1)
    elePassDZ = (abs(events.Electron.eta) < 1.479) & (abs(events.Electron.dz) < 0.1) | (abs(events.Electron.eta) > 1.479) & (abs(events.Electron.dz) < 0.2)

    # select tight electrons
    electronSelectTight = (
        (events.Electron.pt > 10)
        & (abs(events.Electron.eta) < 2.4)
        & (abs(events.Electron.cutBased) >= 4)
        & eleEtaGap
        & elePassDXY
        & elePassDZ
    ) 

    # select loose electrons
    electronSelectLoose = (
        (events.Electron.pt > 10)
        & (abs(events.Electron.eta) < 2.4)
        & eleEtaGap
        & (events.Electron.cutBased >= 1)
        & elePassDXY
        & elePassDZ
        & np.invert(electronSelectTight)
    )

    return events.Electron[electronSelectTight], events.Electron[electronSelectLoose]

def selectTaus(events):

    # bitmask 1 = VVVLoose, 2 = VVLoose, 4 = VLoose, 8 = Loose, 16 = Medium, 32 = Tight, 64 = VTight, 128 = VVTight 

    tauSelectionMedium = (
        (events.Tau.pt > 20.) & (abs(events.Tau.eta) < 2.1)
        & (events.Tau.idDeepTau2017v2p1VSe > 1) 
        & (events.Tau.idDeepTau2017v2p1VSmu > 1) 
        & (events.Tau.idDeepTau2017v2p1VSjet > 15)       
        & (abs(events.Tau.dz) < 0.2)
    ) 

    tauSelectionLoose = (
        (events.Tau.pt > 20.) & (abs(events.Tau.eta) < 2.1)
        & (events.Tau.idDeepTau2017v2p1VSe > 1) 
        & (events.Tau.idDeepTau2017v2p1VSmu > 1) 
        & (events.Tau.idDeepTau2017v2p1VSjet > 7)
        & (abs(events.Tau.dz) < 0.2)
        & np.invert(tauSelectionMedium)
    ) 

    tauSelectionVLoose = (
        (events.Tau.pt > 20.) & (abs(events.Tau.eta) < 2.3)
        & (events.Tau.idDeepTau2017v2p1VSe > 1) 
        & (events.Tau.idDeepTau2017v2p1VSmu > 1) 
        & (events.Tau.idDeepTau2017v2p1VSjet > 3)        
        & (abs(events.Tau.dz) < 0.2)
        & np.invert(tauSelectionLoose)
    )  

    return events.Tau[tauSelectionVLoose], events.Tau[tauSelectionLoose], events.Tau[tauSelectionMedium]


class VLLProcessor(processor.ProcessorABC):

    def __init__(self, isMC=False, era="2017"):
        ################################
        # INITIALIZE COFFEA PROCESSOR
        ################################
        ak.behavior.update(nanoaod.behavior)
        self.isMC = isMC
        self.era = era

        # Categories
        dataset_axis = hist.axis.StrCategory([], growth=True, name="dataset", label="Dataset")
        lep_axis = hist.axis.StrCategory([], growth=True, name="lepFlavor", label="Lepton Flavor")
        tau_axis = hist.axis.StrCategory([], growth=True, name="tauMultiplicity", label="Tau Multiplicity")
        btag_axis = hist.axis.StrCategory([], growth=True,name="bJetMultiplicity", label="bJet Multiplicity")
        systematic_axis = hist.axis.StrCategory([], growth=True, name="systematic", label="Systematic Uncertainty")
        cutflow_axis = hist.axis.StrCategory([], growth=True, name="cutflow",label="Cutflow")

        # Variables
        pt_axis = hist.axis.Regular(200, 0.0, 10000, name="pt", label=r"$p_{T}$ [GeV]")
        eta_axis = hist.axis.Regular(300, -1.5, 1.5, name="eta", label=r"$\eta_{\gamma}$")
        met_pt_axis = hist.axis.Regular(50, 0.0, 500, name="met", label=r"MET [GeV]")
        lep_pt_axis = hist.axis.Regular(30, 0.0, 300, name="lep_pt", label=r"$p_{T}$ [GeV]")
        lep_mll_axis = hist.axis.Regular(30, 0.0, 300, name="lep_mass", label=r"$m_{ll}$ [GeV]")
        event_HT_axis = hist.axis.Regular(50, 0.0, 1000, name="event_HT", label=r"$H_{T}$ [GeV]")
        cut = hist.axis.Regular(14, 0, 14, name="cut", label=r"Cutflow")

        # Book histos
        self.make_output = lambda: {
            "EventCount": processor.value_accumulator(int),
            "MET_pt": hist.Hist(dataset_axis, met_pt_axis,lep_axis,tau_axis,btag_axis,systematic_axis,),
            "lead_lep_pt": hist.Hist(dataset_axis,lep_pt_axis,lep_axis,tau_axis,btag_axis,systematic_axis,),
            "sublead_lep_pt": hist.Hist(dataset_axis,lep_pt_axis,lep_axis,tau_axis,btag_axis,systematic_axis,),
            "lep_mll": hist.Hist(dataset_axis,lep_mll_axis,lep_axis,tau_axis,btag_axis,systematic_axis,),
            "event_HT": hist.Hist(dataset_axis,event_HT_axis,lep_axis,tau_axis,btag_axis,systematic_axis,),
            "event_bjet_HT": hist.Hist(dataset_axis,event_HT_axis,lep_axis,tau_axis,btag_axis,systematic_axis,),
            "lead_jet_pT": hist.Hist(dataset_axis,pt_axis,lep_axis,tau_axis,btag_axis,systematic_axis,),
            "sub_jet_pT": hist.Hist(dataset_axis,pt_axis,lep_axis,tau_axis,btag_axis,systematic_axis,),
            "lead_bjet_pT": hist.Hist(dataset_axis,pt_axis,lep_axis,tau_axis,btag_axis,systematic_axis,),
            "sub_bjet_pT": hist.Hist(dataset_axis,pt_axis,lep_axis,tau_axis,btag_axis,systematic_axis,),
            "CutFlow": hist.Hist(dataset_axis,cut,),
        }

    @property
    def accumulator(self):
        return self._accumulator

    def process(self, events):
        shift_systs = [None]
        if self.isMC:
            shift_systs += ["JESUp", "JESDown", "JERUp", "JERDown"]

        return processor.accumulate(self.process_shift(events, name) for name in shift_systs)

    def process_shift(self, events, shift_syst=None):

        dataset = events.metadata["dataset"]
        output = self.make_output()

        ##################
        # OBJECT SELECTION
        ##################

        # muon and electron selections are broken out into standalone functions
        tightMuons, looseMuons = selectMuons(events)
        tightElectrons, looseElectrons = selectElectrons(events)
        mediumTaus, looseTaus, verylooseTaus = selectTaus(events)

        ## Jet objects
        jets = events.Jet

        # create a processor Weights object, with the same length as the number of events in the chunk
        weights = Weights(len(events))

        if self.isMC:
            events["Jet", "pt_raw"] = (1 - events.Jet.rawFactor) * events.Jet.pt
            events["Jet", "mass_raw"] = (1 - events.Jet.rawFactor) * events.Jet.mass
            events["Jet", "pt_gen"] = ak.values_astype(ak.fill_none(events.Jet.matched_gen.pt, 0), np.float32)
            events["Jet", "rho"] = ak.broadcast_arrays(events.fixedGridRhoFastjetAll, events.Jet.pt)[0]
            events_cache = events.caches[0]
            jet_factory = get_jet_factory(self.era)
            corrected_jets = jet_factory.build(events.Jet, lazy_cache=events_cache)
            
            # If processing a jet systematic, we need to update the
            # jets to reflect the jet systematic uncertainty variations
            if shift_syst == "JERUp":
                jets = corrected_jets.JER.up 
            elif shift_syst == "JERDown":
                jets = corrected_jets.JER.down
            elif shift_syst == "JESUp":
                jets = corrected_jets.JES_jes.up
            elif shift_syst == "JESDown":
                jets = corrected_jets.JES_jes.down
            else:
                # either nominal or some shift systematic unrelated to jets
                jets = corrected_jets

            if False:
                print(" pt ", events.Jet.pt[0], "  pt corrected ", jets.pt[0])
                print(" ")

        ## More cross-cleaning: check jet does not overlap with our selected leptons
        jetMuMask = ak.all(jets.metric_table(tightMuons) > 0.4, axis=-1)
        jetEleMask = ak.all(jets.metric_table(tightElectrons) > 0.4, axis=-1)
        jetTauMask = ak.all(jets.metric_table(mediumTaus) > 0.4, axis=-1)

        mediumJetIDbit = 1

        tightJet = jets[
            (abs(jets.eta) < 2.4)
            & (jets.pt > 30)
            & ((jets.jetId >> mediumJetIDbit & 1) == 1)
            & jetMuMask
            & jetEleMask
            & jetTauMask
        ]

        # label the subset of tightJet which pass the DeepJet tagger
        # https://btv-wiki.docs.cern.ch/ScaleFactors/#sf-campaigns

        bTagWP = 0.3040  # 2017 DeepJet working point
        if self.era == "2018":
            bTagWP = 0.2783  # 2018 DeepJet working point
        if self.era == "2016pre":
            bTagWP = 0.2598  # 2016 preVFP DeepJet working point
        if self.era == "2016post":
            bTagWP = 0.2489  # 2016 preVFP DeepJet working point

        tightJet["btagged"] = tightJet.btagDeepFlavB > bTagWP 

        # Sorted leptons pT 
        leadingMuon = ak.firsts(tightMuons)
        leadingElectron = ak.firsts(tightElectrons)
        leptons = ak.concatenate((tightMuons,tightElectrons),axis = 1)
        leptons = leptons[ak.argsort(-leptons.pt, axis=1)]
        dileptons = leptons.mask[ak.num(leptons) >= 2][:,0] + leptons.mask[ak.num(leptons) >= 2][:,1]
        # Usefull jet dict for applying cuts
        fourJets = tightJet.mask[ak.num(tightJet) >= 4]
        bjets = tightJet.mask[ak.sum(tightJet.btagged, axis=-1) > 1]
        leadingbjet = ak.firsts(bjets)

        ##################
        # EVENT VARIABLES
        ##################
        event_HT = ak.sum(tightJet.pt,-1)
        event_bHT = ak.sum(tightJet.pt[tightJet.btagged],-1)

        #####################
        # EVENT SELECTION
        #####################

        selection = PackedSelection()

        ## MET filters 
        selection.add("filters", (events.Flag.goodVertices) & (events.Flag.globalSuperTightHalo2016Filter) & (events.Flag.HBHENoiseFilter) & (events.Flag.HBHENoiseIsoFilter) & 
                    (events.Flag.EcalDeadCellTriggerPrimitiveFilter) & (events.Flag.BadPFMuonFilter) & (((not (self.isMC)) & events.Flag.eeBadScFilter) | (self.isMC)) )

        ## Trigger selection
        # Select events from the SingleElectron dataset with the single e HLT path
        # Select events from the SingleMuon dataset with the single mu HLT path & veto the single e HLT path
        # https://twiki.cern.ch/twiki/bin/viewauth/CMS/EgHLTScaleFactorMeasurements
        if self.era == "2016pre":
            elehlt = events.HLT.Ele27_WPTight_Gsf
        elif self.era == "2016post":
            elehlt = events.HLT.Ele27_WPTight_Gsf
        elif self.era == "2017":
            elehlt = events.HLT.Ele32_WPTight_Gsf_L1DoubleEG
        else:            
            elehlt = events.HLT.Ele32_WPTight_Gsf

        muhlt = events.HLT.IsoMu24
        muhltSel = muhlt & elehlt
        selection.add("eleTrigger", ( (elehlt & (self.isMC)) | (elehlt & (not(self.isMC)) & (not "Mu" in dataset)) ) )
        selection.add("muTrigger", ( (muhltSel & (self.isMC)) | (muhltSel & (not(self.isMC)) & ("Mu" in dataset)) ) )

        # (the ak.num() method returns the number of objects in each row of a jagged array)
        selection.add("twoMuon", (ak.num(tightMuons) == 2) & (leadingMuon.pt > 30.) )
        selection.add("oneMuon", (ak.num(tightMuons) == 1) & ( (leadingElectron.pt > 30) | (leadingMuon.pt > 30) ) )
        selection.add("zeroMuon", ak.num(tightMuons) == 0)
        selection.add("zeroLooseMuon", ak.num(looseMuons) == 0) 

        # similar selections will be needed for electrons
        selection.add("twoEle", ak.num(tightElectrons) == 2) 
        selection.add("oneEle", (ak.num(tightElectrons) == 1) & ( (leadingElectron.pt > 30) | (leadingMuon.pt > 30) ) )  
        selection.add("zeroEle", ak.num(tightElectrons) == 0) 
        selection.add("zeroLooseEle", ak.num(looseElectrons) == 0)

        # similar selections will be needed for taus
        selection.add("twoTau", ak.num(mediumTaus) == 2) 
        selection.add("oneTau", ak.num(mediumTaus) == 1)  
        selection.add("zeroTau", ak.num(mediumTaus) == 0)  

        selection.add("zpeak",(ak.num(leptons) == 2) & ((dileptons.mass <76.) | (dileptons.mass >106.))) 

        # di-lepton categories
        dimuon_cat = {"twoMuon","zeroEle"}
        diele_cat = {"twoEle","zeroMuon"}
        elemuon_cat = {"oneEle","oneMuon"}

        selection.add("eeSel", selection.all(*diele_cat))
        selection.add("emuSel", selection.all(*elemuon_cat))
        selection.add("mumuSel", selection.all(*dimuon_cat))

        #  Jet selection: One which selects events with at least 4 tightJet and at least # b-tagged jet
        selection.add("jetSel_4j2b",(ak.num(tightJet) >= 4) & (ak.sum(tightJet.btagged, axis=-1) == 2),)
        selection.add("jetSel_4j3b",(ak.num(tightJet) >= 4) & (ak.sum(tightJet.btagged, axis=-1) == 3),)
        selection.add("jetSel_4j4b",(ak.num(tightJet) >= 4) & (ak.sum(tightJet.btagged, axis=-1) >= 4),)

        selection.add("evt_sel",((selection.all("filters")) & (( selection.all("eleTrigger")) | (selection.all("muTrigger"))) 
                                & (events.MET.pt>40.) & (ak.num(leptons) == 2) & (dileptons.mass >20.) 
                                & (event_HT >300.) & (fourJets.pt[:,0]>100.) & (fourJets.pt[:,1]>50.)),)

        # useful debugger for selection efficiency
        if False and shift_syst is None:
            print(dataset)
            for n in selection.names:
                print("- Cut ",n," pass ", selection.all(n).sum()," of ", len(events)," events")

        if shift_syst is None:
            selection1 = PackedSelection()        
            selection1.add("0", (ak.num(tightJet) >= 0) ) # Dummy for no selection
            selection1.add("1", (selection.all("eleTrigger")) | (selection.all("muTrigger")) )
            selection1.add("2", (selection1.all("1")) & (ak.num(tightJet) >= 4) )
            selection1.add("3", (selection1.all("2")) & (ak.num(leptons) == 2) )
            selection1.add("4", (selection1.all("3")) & (dileptons.mass >20.))
            selection1.add("5", (selection1.all("4")) & ((ak.firsts(leptons)).pt >30.))
            selection1.add("6", (selection1.all("5")) & (events.MET.pt>40.))
            selection1.add("7", (selection1.all("6")) &(fourJets.pt[:,0]>100.))
            selection1.add("8", (selection1.all("7")) &(fourJets.pt[:,1]>50.))
            selection1.add("9", (selection1.all("8")) & (event_HT >300.))
            selection1.add("10", (selection1.all("9")) & (event_bHT>150.)) # Is this really needed? 

            bin=0
            for n in selection1.names:
                output["CutFlow"].fill(
                    dataset=dataset,
                    cut=np.asarray(bin),
                    weight=selection1.all(n).sum(),
                )
                bin = bin+1 
            
        #####################
        # WEIGHTS AND SFS
        #####################

        if(self.isMC):

            ##################
            # Electron SFs
            ##################

            eleID = ele_id_sf(tightElectrons.eta, tightElectrons.pt)
            eleIDerr = ele_id_err(tightElectrons.eta, tightElectrons.pt)
            eleRECO = ele_reco_sf(tightElectrons.eta, tightElectrons.pt)
            eleRECOerr = ele_reco_err(tightElectrons.eta, tightElectrons.pt)

            eleSF = ak.prod((eleID * eleRECO), axis=-1)
            eleSF_up = ak.prod(((eleID + eleIDerr) * (eleRECO + eleRECOerr)), axis=-1)
            eleSF_down = ak.prod(((eleID - eleIDerr) * (eleRECO - eleRECOerr)), axis=-1) 
            weights.add("eleEffWeight", weight=eleSF, weightUp=eleSF_up, weightDown=eleSF_down)  

            ##################
            # btag SFs
            ##################
            # Taken from here 
            # https://cms-nanoaod-integration.web.cern.ch/commonJSONSFs/summaries/BTV_2016preVFP_UL_btagging.html

            jetSFs = btagSF(tightJet, syst="central")
            btagWeight = ak.prod(jetSFs, axis=-1)
            btag_syts = ['cferr1', 'cferr2', 'hf', 'hfstats1', 'hfstats2', 'lf', 'lfstats1', 'lfstats2'] 
        
            btagWeight_down,btagWeight_up = 1, 1        
            for bsyst in btag_syts:
                j = tightJet[tightJet.hadronFlavour != 4]
                if(bsyst=='cferr1' or bsyst=='cferr2'):
                    j = tightJet[tightJet.hadronFlavour==4]
                jetSFs_up = btagSF(j, syst="up_"+bsyst)
                jetSFs_down = btagSF(j, syst="down_"+bsyst)
                partial_btagWeight_up = ak.prod(jetSFs_up, axis=-1)
                partial_btagWeight_down = ak.prod(jetSFs_down, axis=-1)
                weights.add("btagVariation_"+bsyst,weight=np.ones(len(partial_btagWeight_up)),weightUp=partial_btagWeight_up,weightDown=partial_btagWeight_down,)
            
            weights.add("btagWeight",weight=btagWeight,weightUp=btagWeight,weightDown=btagWeight,) # Add the SF as up and down variation


            ##################
            # PU SFs
            ##################
            if False:
                print(GetPUSF(events.Pileup.nTrueInt,self.era,'nominal'))
            puWeight = GetPUSF(events.Pileup.nTrueInt,self.era,'nominal')
            puWeight_Up = GetPUSF(events.Pileup.nTrueInt,self.era,'up')
            puWeight_Down = GetPUSF(events.Pileup.nTrueInt,self.era,'down')
            weights.add("puWeight",weight=puWeight,weightUp=puWeight_Up,weightDown=puWeight_Down,)


            # This section sets up some of the weight shifts related to theory uncertainties
            # in some samples, generator systematics are not available, in those case the systematic weights of 1. are used
            '''
            if ak.mean(ak.num(events.PSWeight)) == 1:
                weights.add(
                    "ISR",
                    weight=np.ones(len(events)),
                    weightUp=np.ones(len(events)),
                    weightDown=np.ones(len(events)),
                )
                weights.add(
                    "FSR",
                    weight=np.ones(len(events)),
                    weightUp=np.ones(len(events)),
                    weightDown=np.ones(len(events)),
                )
                weights.add(
                    "PDF",
                    weight=np.ones(len(events)),
                    weightUp=np.ones(len(events)),
                    weightDown=np.ones(len(events)),
                )

            # Otherwise, calculate the weights and systematic variations
            else:
                # ISR / FSR uncertainty weights
                if not ak.all(
                    events.Generator.weight == events.LHEWeight.originalXWGTUP
                ):
                    psWeights = (
                        events.PSWeight
                        * events.LHEWeight.originalXWGTUP
                        / events.Generator.weight
                    )
                else:
                    psWeights = events.PSWeight

                weights.add(
                    "ISR",
                    weight=np.ones(len(events)),
                    weightUp=psWeights[:, 2],
                    weightDown=psWeights[:, 0],
                )
                weights.add(
                    "FSR",
                    weight=np.ones(len(events)),
                    weightUp=psWeights[:, 3],
                    weightDown=psWeights[:, 1],
                )
        '''

        ###################
        # FILL HISTOGRAMS
        ###################

        systList = []
        if self.isMC:
            if shift_syst is None:
                systList = [
                    "nominal",
                    "muEffWeightUp",
                    "muEffWeightDown",
                    "eleEffWeightUp",
                    "eleEffWeightDown",
                    "ISRUp",
                    "ISRDown",
                    "FSRUp",
                    "FSRDown",
                    "PDFUp",
                    "PDFDown",
                    "puWeightUp",
                    "puWeightDown",
                    "btagWeightUp",
                    "btagWeightDown",
                ]
            else:
                systList = [shift_syst]
        else:
            systList = ["noweight"]

        for syst in systList:

            # find the event weight to be used when filling the histograms
            weightSyst = syst

            # in the case of 'nominal', or the jet energy systematics, no weight systematic variation is used (weightSyst=None)
            if syst in ["nominal", "JERUp", "JERDown", "JESUp", "JESDown"]:
                weightSyst = None

            if syst == "noweight":
                evtWeight = np.ones(len(events))
            else:
                # call weights.weight() with the name of the systematic to be varied
                evtWeight = weights.weight(weightSyst)

            for tauSelec in ["zeroTau", "oneTau","twoTau"]:

                evtsel_4j2t = { 'ee': selection.all("eeSel", "zpeak", "jetSel_4j2b", "evt_sel", tauSelec),
                                'emu': selection.all("emuSel", "jetSel_4j2b", "evt_sel", tauSelec),
                                'mumu': selection.all("mumuSel","zpeak", "jetSel_4j2b", "evt_sel", tauSelec),
                            }

                evtsel_4j3t = { 'ee': selection.all("eeSel", "zpeak", "jetSel_4j3b", "evt_sel", tauSelec),
                                'emu': selection.all("emuSel", "jetSel_4j3b", "evt_sel", tauSelec),
                                'mumu': selection.all("mumuSel", "zpeak", "jetSel_4j3b", "evt_sel", tauSelec),
                            }

                evtsel_4j4t = { 'ee': selection.all("eeSel", "zpeak", "jetSel_4j4b", "evt_sel", tauSelec),
                                'emu': selection.all("emuSel", "jetSel_4j4b", "evt_sel", tauSelec),
                                'mumu': selection.all("mumuSel", "zpeak", "jetSel_4j4b", "evt_sel", tauSelec),
                            }

                # 4j2b region
                for lepton in evtsel_4j2t.keys():
                    output["MET_pt"].fill(
                        dataset=dataset,
                        met=np.asarray(events.MET.pt[evtsel_4j2t[lepton]]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j2b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j2t[lepton]],
                    )

                    output["lead_lep_pt"].fill(
                        dataset=dataset,
                        lep_pt=np.asarray(leptons.pt[evtsel_4j2t[lepton]][:,0]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j2b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j2t[lepton]],
                    )

                    output["sublead_lep_pt"].fill(
                        dataset=dataset,
                        lep_pt=np.asarray(leptons.pt[evtsel_4j2t[lepton]][:,1]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j2b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j2t[lepton]],
                    )

                    output["lep_mll"].fill(
                        dataset=dataset,
                        lep_mass=np.asarray(dileptons[evtsel_4j2t[lepton]].mass),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j2b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j2t[lepton]],
                    )                
                
                    output["event_HT"].fill(
                        dataset=dataset,
                        event_HT=np.asarray(event_HT[evtsel_4j2t[lepton]]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j2b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j2t[lepton]],
                    )     
                
                    output["event_bjet_HT"].fill(
                        dataset=dataset,
                        event_HT=np.asarray(event_bHT[evtsel_4j2t[lepton]]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j2b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j2t[lepton]],
                    )  

                    output["lead_jet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(tightJet.pt[evtsel_4j2t[lepton]][:,0]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j2b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j2t[lepton]],
                    )  

                    output["sub_jet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(tightJet.pt[evtsel_4j2t[lepton]][:,1]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j2b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j2t[lepton]],
                    )  

                    output["lead_bjet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(bjets.pt[evtsel_4j2t[lepton]][:,0]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j2b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j2t[lepton]],
                    )  

                    output["sub_bjet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(bjets.pt[evtsel_4j2t[lepton]][:,1]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j2b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j2t[lepton]],
                    )  

                # 4j3b region
                for lepton in evtsel_4j3t.keys():
                    output["MET_pt"].fill(
                        dataset=dataset,
                        met=np.asarray(events.MET.pt[evtsel_4j3t[lepton]]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j3b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j3t[lepton]],
                    ) 

                    output["lead_lep_pt"].fill(
                        dataset=dataset,
                        lep_pt=np.asarray(leptons.pt[evtsel_4j3t[lepton]][:,0]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j3b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j3t[lepton]],
                    )

                    output["sublead_lep_pt"].fill(
                        dataset=dataset,
                        lep_pt=np.asarray(leptons.pt[evtsel_4j3t[lepton]][:,1]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j3b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j3t[lepton]],
                    )

                    output["lep_mll"].fill(
                        dataset=dataset,
                        lep_mass=np.asarray(dileptons[evtsel_4j3t[lepton]].mass),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j3b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j3t[lepton]],
                    )                
                
                    output["event_HT"].fill(
                        dataset=dataset,
                        event_HT=np.asarray(event_HT[evtsel_4j3t[lepton]]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j3b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j3t[lepton]],
                    )  

                    output["event_bjet_HT"].fill(
                        dataset=dataset,
                        event_HT=np.asarray(event_bHT[evtsel_4j3t[lepton]]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j3b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j3t[lepton]],                        
                    )  

                    output["lead_jet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(tightJet.pt[evtsel_4j3t[lepton]][:,0]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j3b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j3t[lepton]],
                    )  

                    output["sub_jet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(tightJet.pt[evtsel_4j3t[lepton]][:,1]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j3b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j3t[lepton]],
                    ) 

                    output["lead_bjet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(bjets.pt[evtsel_4j3t[lepton]][:,0]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j3b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j3t[lepton]],
                    )  

                    output["sub_bjet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(bjets.pt[evtsel_4j3t[lepton]][:,1]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j3b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j3t[lepton]],
                    ) 

                # 4j4b region
                for lepton in evtsel_4j4t.keys():
                    output["MET_pt"].fill(
                        dataset=dataset,
                        met=np.asarray(events.MET.pt[evtsel_4j4t[lepton]]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j4b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j4t[lepton]],
                    )

                    # For debugging
                    if False:
                        print(np.asarray( leptons.pt[evtsel_4j4t[lepton]][:,0]) )
                        print(leptons[evtsel_4j4t[lepton]][:,0] )
                        print('1: ',(leptons[evtsel_4j4t[lepton]][:,0] + leptons[evtsel_4j4t[lepton]][:,1]).mass)
                        print('2: ',dileptons[evtsel_4j4t[lepton]].mass)

                    output["lead_lep_pt"].fill(
                        dataset=dataset,
                        lep_pt=np.asarray(leptons.pt[evtsel_4j4t[lepton]][:,0]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j4b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j4t[lepton]],
                    )

                    output["sublead_lep_pt"].fill(
                        dataset=dataset,
                        lep_pt=np.asarray(leptons.pt[evtsel_4j4t[lepton]][:,1]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j4b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j4t[lepton]],
                    )

                    output["lep_mll"].fill(
                        dataset=dataset,
                        lep_mass=np.asarray(dileptons[evtsel_4j4t[lepton]].mass),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j4b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j4t[lepton]],
                    )                
                
                    output["event_HT"].fill(
                        dataset=dataset,
                        event_HT=np.asarray(event_HT[evtsel_4j4t[lepton]]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j4b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j4t[lepton]],
                    )     
                
                    output["event_bjet_HT"].fill(
                        dataset=dataset,
                        event_HT=np.asarray(event_bHT[evtsel_4j4t[lepton]]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j4b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j4t[lepton]],
                    )  

                    output["lead_jet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(tightJet.pt[evtsel_4j4t[lepton]][:,0]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j4b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j4t[lepton]],
                    )  

                    output["sub_jet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(tightJet.pt[evtsel_4j4t[lepton]][:,1]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j4b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j4t[lepton]],
                    )  

                    output["lead_bjet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(bjets.pt[evtsel_4j4t[lepton]][:,0]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j4b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j4t[lepton]],
                    )  

                    output["sub_bjet_pT"].fill(
                        dataset=dataset,
                        pt=np.asarray(bjets.pt[evtsel_4j4t[lepton]][:,1]),
                        lepFlavor=lepton,
                        tauMultiplicity=tauSelec,
                        bJetMultiplicity="4j4b",
                        systematic=syst,
                        weight=evtWeight[evtsel_4j4t[lepton]],
                    ) 

                output["EventCount"] = len(events)

            return {dataset:output}
        
    def postprocess(self, accumulator):
        return accumulator
