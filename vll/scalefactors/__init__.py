"""Scale factors for the analysis
This module loads and sets up the scale factor objects
"""
import os.path
import correctionlib

from coffea import util
from coffea.btag_tools import BTagScaleFactor
from coffea.jetmet_tools import CorrectedJetsFactory, JECStack
from coffea.lookup_tools import extractor

cwd = os.path.dirname(__file__)

ceval2016pre = correctionlib.CorrectionSet.from_file(f"{cwd}/BTag/2016preVFP_UL/btagging.json")
ceval2016post = correctionlib.CorrectionSet.from_file(f"{cwd}/BTag/2016postVFP_UL/btagging.json")
ceval2017 = correctionlib.CorrectionSet.from_file(f"{cwd}/BTag/2017_UL/btagging.json")
ceval2018 = correctionlib.CorrectionSet.from_file(f"{cwd}/BTag/2018_UL/btagging.json")


# TO FIX
ele_id_sf = util.load(f"{cwd}/EleMu/ele_id_sf.coffea")
ele_id_err = util.load(f"{cwd}/EleMu/ele_id_err.coffea")

ele_reco_sf = util.load(f"{cwd}/EleMu/ele_reco_sf.coffea")
ele_reco_err = util.load(f"{cwd}/EleMu/ele_reco_err.coffea")

#mu_id_sf = util.load(f"{cwd}/MuEGammaScaleFactors/mu_id_sf.coffea")
#mu_id_err = util.load(f"{cwd}/MuEGammaScaleFactors/mu_id_err.coffea")

#mu_iso_sf = util.load(f"{cwd}/MuEGammaScaleFactors/mu_iso_sf.coffea")
#mu_iso_err = util.load(f"{cwd}/MuEGammaScaleFactors/mu_iso_err.coffea")

#mu_trig_sf = util.load(f"{cwd}/MuEGammaScaleFactors/mu_trig_sf.coffea")
#mu_trig_err = util.load(f"{cwd}/MuEGammaScaleFactors/mu_trig_err.coffea")
