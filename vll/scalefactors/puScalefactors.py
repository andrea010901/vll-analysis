
# Code was taken from here :
# https://github.com/TopEFT/topcoffea/blob/239dda756cf12118b87953caf1ccf169bd160a2b/topcoffea/modules/corrections.py

###### Pileup reweighing
##############################################
## Get central PU data and MC profiles and calculate reweighting
## Using the current UL recommendations in:
##   https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData
##   - 2018: /afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PileUp/UltraLegacy/
##   - 2017: /afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/PileUp/UltraLegacy/
##   - 2016: /afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/PileUp/UltraLegacy/
##
## MC histograms from:
##    https://github.com/CMS-LUMI-POG/PileupTools/

import os.path
import uproot
from coffea import lookup_tools
import numpy as np

cwd = os.path.dirname(__file__)
pudirpath = f'{cwd}/pileup/'

pu_w = 69200
pu_w_up = 72400
pu_w_down = 66000

def GetDataPUname(era, var=0):
  ''' Returns the name of the file to read pu observed distribution '''
  if era == '2016pre': era = '2016-preVFP'
  if era == '2016post'   : era = "2016-postVFP"
  if   var== 'nominal': ppxsec = pu_w
  elif var== 'up': ppxsec = pu_w_up
  elif var== 'down': ppxsec = pu_w_down

  return 'PileupHistogram-goldenJSON-13tev-%s-%sub-99bins.root'%((era), str(ppxsec))

MCPUfile = {'2016pre':'pileup_2016BF.root', '2016post':'pileup_2016GH.root', '2017':'pileup_2017_shifts.root', '2018':'pileup_2018_shifts.root'}

def GetMCPUname(era):
  ''' Returns the name of the file to read pu MC profile '''
  return MCPUfile[str(era)]

PUfunc = {}
### Load histograms and get lookup tables (extractors are not working here...)
for era in ['2016pre','2016post','2017','2018']:
  PUfunc[era] = {}
  with uproot.open(pudirpath+GetMCPUname(era)) as fMC:
    hMC = fMC['pileup']
    PUfunc[era]['MC'] = lookup_tools.dense_lookup.dense_lookup(hMC.values() / np.sum( hMC.values()), hMC.axis(0).edges())
  with uproot.open(pudirpath+GetDataPUname(era,  'nominal')) as fData:
    hD   = fData  ['pileup']
    PUfunc[era]['Data'  ] = lookup_tools.dense_lookup.dense_lookup(hD.values() / np.sum(hD.values()), hD.axis(0).edges())
  with uproot.open(pudirpath+GetDataPUname(era,  'up')) as fDataUp:
    hDUp = fDataUp['pileup']
    PUfunc[era]['DataUp'] = lookup_tools.dense_lookup.dense_lookup(hDUp.values() / np.sum(hDUp.values()), hD.axis(0).edges())
  with uproot.open(pudirpath+GetDataPUname(era, 'down')) as fDataDo:
    hDDo = fDataDo['pileup']
    PUfunc[era]['DataDo'] = lookup_tools.dense_lookup.dense_lookup(hDDo.values() / np.sum(hDDo.values()), hD.axis(0).edges())


def GetPUSF(nTrueInt, era, var='nominal'):
  if era not in ['2016pre','2016post','2017','2018']: raise Exception(f"Error: Unknown era \"{era}\".")
  nMC  =PUfunc[era]['MC'](nTrueInt+1)
  nData=PUfunc[era]['DataUp' if var == 'up' else ('DataDo' if var == 'down' else 'Data')](nTrueInt)
  weights = np.divide(nData,nMC)
  return weights
