import os.path
import correctionlib

from coffea import util
from coffea.jetmet_tools import CorrectedJetsFactory, JECStack
from coffea.lookup_tools import extractor


def get_jet_factory(era):

    cwd = os.path.dirname(__file__)

    if era=='2016pre': 
        jec_tag='Summer19UL16APV_V7'; jer_tag='Summer20UL16APV_JRV3'
    elif era=='2016post': 
        jec_tag='Summer19UL16_V7'; jer_tag='Summer20UL16_JRV3'
    elif era=='2017': 
        jec_tag='Summer19UL17_V5'; jer_tag='Summer19UL17_JRV2'
    elif era=='2018': 
        jec_tag='Summer19UL18_V5'; jer_tag='Summer19UL18_JRV2'
    else: 
        raise Exception(f"Error: Unknown era \"{era}\".")


    if(not os.path.isfile(f"{cwd}/JEC/"+era+"/"+jec_tag+"_MC_L1FastJet_AK4PFchs.jec.txt")):
        print(os.path.isfile(f"{cwd}/JEC/"+era+"/"+jec_tag+"_MC_L1FastJet_AK4PFchs.jec.txt"))
        raise Exception(f"Error: Unknown JEC file path.")

    
    # UL JEC/JER files taken from :
    # https://twiki.cern.ch/twiki/bin/view/CMS/JECDataMC#2018_Data
    # https://twiki.cern.ch/twiki/bin/view/CMS/JetResolution

    Jetext = extractor()
    Jetext.add_weight_sets(
        [
            f"* * {cwd}/JEC/"+era+"/"+jec_tag+"_MC_L1FastJet_AK4PFchs.jec.txt",
            f"* * {cwd}/JEC/"+era+"/"+jec_tag+"_MC_L2Relative_AK4PFchs.jec.txt",
            f"* * {cwd}/JEC/"+era+"/"+jec_tag+"_MC_Uncertainty_AK4PFchs.junc.txt",
            f"* * {cwd}/JEC/"+era+"/"+jer_tag+"_MC_PtResolution_AK4PFchs.jr.txt",
            f"* * {cwd}/JEC/"+era+"/"+jer_tag+"_MC_SF_AK4PFchs.jersf.txt",
        ]
    )
    Jetext.finalize()
    Jetevaluator = Jetext.make_evaluator()

    jec_names = [
        jec_tag+"_MC_L1FastJet_AK4PFchs",
        jec_tag+"_MC_L2Relative_AK4PFchs",
        jec_tag+"_MC_Uncertainty_AK4PFchs",
        jer_tag+"_MC_PtResolution_AK4PFchs",
        jer_tag+"_MC_SF_AK4PFchs",
    ]

    jec_inputs = {name: Jetevaluator[name] for name in jec_names}
    jec_stack = JECStack(jec_inputs)

    name_map = jec_stack.blank_name_map
    name_map["JetPt"] = "pt"
    name_map["JetMass"] = "mass"
    name_map["JetEta"] = "eta"
    name_map["JetA"] = "area"
    name_map["ptGenJet"] = "pt_gen"
    name_map["ptRaw"] = "pt_raw"
    name_map["massRaw"] = "mass_raw"
    name_map["Rho"] = "rho"

    jet_factory = CorrectedJetsFactory(name_map, jec_stack)
   
    return jet_factory