import time
from distributed import Client
from dask_jobqueue import HTCondorCluster
import os
import sys

import hist
from coffea import processor, nanoevents, util
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema
import awkward as ak
from vll.processor import VLLProcessor
from vll.utils.crossSections import lumis, crossSections, nevents

import argparse
import uproot
from filelists.fileset import *
from filelists.fileset_SingleTop import *

def move_X509():
    try:
        _x509_localpath = (
            [
                line
                for line in os.popen("voms-proxy-info").read().split("\n")
                if line.startswith("path")
            ][0]
            .split(":")[-1]
            .strip()
        )
    except Exception as err:
        raise RuntimeError(
            "x509 proxy could not be parsed, try creating it with 'voms-proxy-init'"
        ) from err
    _x509_path = f'/scratch/{os.environ["USER"]}/{_x509_localpath.split("/")[-1]}'
    os.system(f"cp {_x509_localpath} {_x509_path}")
    return os.path.basename(_x509_localpath)


if __name__ == "__main__":

    tic = time.time()
    os.environ["CONDOR_CONFIG"] = "/etc/condor/condor_config"
    listOfFiles = filesetOther

    _x509_path = move_X509()
    
    cluster = HTCondorCluster(
        cores=1,
        memory="2 GB",
        disk="1 GB",
        job_extra={
            "log": "dask_job_output.$(PROCESS).$(CLUSTER).log",
            "output": "dask_job_output.$(PROCESS).$(CLUSTER).out",
            "error": "dask_job_output.$(PROCESS).$(CLUSTER).err",
            "should_transfer_files": "yes",
            "when_to_transfer_output": "ON_EXIT_OR_EVICT",
            "+SingularityImage": '"/cvmfs/unpacked.cern.ch/registry.hub.docker.com/coffeateam/coffea-dask-cc7:latest"',
            "Requirements": "HasSingularityJobStart",
            #"request_GPUs" : "1",
            "InitialDir": f'/scratch/{os.environ["USER"]}',
            "transfer_input_files": f'{os.environ["EXTERNAL_BIND"]}/.env,{_x509_path},{os.environ["EXTERNAL_BIND"]}/vll'
        },
        env_extra=[
            "export XRD_RUNFORKHANDLER=1",
            f"export X509_USER_PROXY={_x509_path}",
        ]
    )
    cluster.adapt(minimum=1, maximum=400)
    client = Client(cluster)

    exe_args = {
        "client": client,
        "savemetrics": True,
        "schema": nanoevents.NanoAODSchema,
        "align_clusters": True,
        "skipbadfiles": True,
        "xrootdtimeout": 1000,
    }

    print("Waiting for at least one worker...")
    client.wait_for_workers(1)
    print("Found one")

    era = 2017

    output, metrics = processor.run_uproot_job(
        listOfFiles,
        treename="Events",
        processor_instance=VLLProcessor(isMC=True,era=era),
        executor=processor.dask_executor,
        executor_args=exe_args,
        chunksize=20000,
        maxchunks=None,
    )
    
    
    # Compute original number of events for normalization
    output["InputEventCount"] = processor.defaultdict_accumulator(int)
    lumi_sfs = {}
    for dataset_name,dataset_files in listOfFiles.items():
        # Calculate luminosity scale factor
        lumi_sfs[dataset_name] = (
            crossSections[dataset_name]
            * lumis[era]
            / nevents[era][dataset_name]
        )
        print(dataset_name,":",nevents[era][dataset_name])

    for key, obj in output.items():
        if isinstance(obj, hist.Hist):
            obj.scale(lumi_sfs, axis="dataset")
    

    elapsed = time.time() - tic
    print("Total time: %.1f seconds"%elapsed)
    print("Total rate: %.1f events / second"%(output['EventCount'].value/elapsed))

    print(f"Output: {output}")
    print(f"Metrics: {metrics}")
    print(f"Finished in {elapsed:.1f}s")
    print(f"Events/s: {metrics['entries'] / elapsed:.0f}")

    outfile = "output_MC"+f'{listOfFiles}'.split(':')[0][2:-1]+".coffea"
    print(outfile)    
    util.save(output, outfile)
    print(f"Saved output to {outfile}")
