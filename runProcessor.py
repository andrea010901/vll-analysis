from coffea import util, processor
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema
from vll.processor import VLLProcessor
from vll.utils.crossSections import lumis, crossSections, nevents
import awkward as ak
import hist
import time
import time
import sys
import os
import argparse
import uproot
from filelists.fileset import *
from testData import *

uproot.open.defaults["xrootd_handler"] = uproot.source.xrootd.MultithreadedXRootDSource

# Initialize these :
#---
era = "2018"
listOfFiles = filesetSignal 
isMC = True
#---

tstart = time.time()
#outfile = "output_MC"+f'{listOfFiles}'.split(':')[0][2:-1]+".coffea"
outfile = "test.coffea"
print('Will create : ',outfile)

output = processor.run_uproot_job(
    listOfFiles,
    "Events",
    VLLProcessor(isMC=isMC,era=era),
    processor.futures_executor, # iterative_executor, futures_executor : Run "multithreaded"
    executor_args={'schema': NanoAODSchema,'workers': 4,'skipbadfiles': True,'xrootdtimeout':300},
    chunksize=10000,
    maxchunks=None,
)

output["EventCount"] = processor.defaultdict_accumulator(int)
for dataset_name,dataset_files in listOfFiles.items():
    # Calculate luminosity scale factor
    lumi_sf = 1.000
    if(isMC):
        lumi_sf = (
            crossSections[dataset_name]
            * lumis[era]
            / nevents[era][dataset_name]
        )
        print(dataset_name,":",nevents[era][dataset_name])

    for key, obj in output[dataset_name].items():
        if isinstance(obj, hist.Hist):
            obj *= lumi_sf

elapsed = time.time() - tstart
print("Total time: %.1f seconds"%elapsed)
util.save(output, outfile)
print(f"Saved output to {outfile}")
