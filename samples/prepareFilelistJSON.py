from dasInterface import dasInterface
import json
import argparse

def main(args):

    with open(args.inputFile, 'r') as readFile:
        datasets = json.load(readFile)
    print(json.dumps(datasets,sort_keys=True,indent=4))

    listOfPaths = []
    listOfEvents = []

    for campaign in datasets:
        for datasetName in datasets[campaign]:
            listOfPaths.append(('/'+datasets[campaign][datasetName]+'/'+campaign+'/'+args.dataTier).strip())
            listOfEvents.append(('/'+datasets[campaign][datasetName]+'/'+campaign+'/'+args.dataTier).strip())
    
    print(listOfPaths)
    print(listOfEvents)
    
    theDasInterface = dasInterface()
    fileList = theDasInterface.getCompleteDictionaryOfFilesFromPathList(DASPaths = listOfPaths)

    with open(args.outputFile, 'w') as writeFile:
        json.dump(fileList, writeFile, indent=4)
    with open(args.outputFile, 'r') as readFile:
        s = readFile.read()
    with open(args.outputFile, 'w') as writeFile:
        for campaign in datasets:
            for datasetName in datasets[campaign]:
                s = s.replace(datasets[campaign][datasetName]+'\":',datasetName+'\":')
        writeFile.write(s)

    neventList = theDasInterface.getCompleteDictionaryOfNumberOfEvents(DASPaths = listOfEvents)
    with open('nEvents_'+args.outputFile, 'w') as writeFileN:
        json.dump(neventList, writeFileN, indent=4)    
    with open('nEvents_'+args.outputFile, 'r') as readFile:
        s = readFile.read()
    with open('nEvents_'+args.outputFile, 'w') as writeFile:
        for campaign in datasets:
            for datasetName in datasets[campaign]:
                s = s.replace(datasets[campaign][datasetName],datasetName)
        writeFile.write(s)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Generate file list JSONs from a list of samples')
    parser.add_argument('--dataTier',nargs = '?', choices=['MINIAODSIM','NANOAODSIM','MINIAOD','NANOAOD'],help='Datatier to form the list of files for',default='NANOAODSIM')
    parser.add_argument('--inputFile',nargs='?',help='Input JSON file that has campaign:{name:dataset} format',required=True)
    parser.add_argument('--outputFile',nargs='?',help='Output file name to dump the final list of file in JSON format',required=True)

    args = parser.parse_args()

    main(args)
