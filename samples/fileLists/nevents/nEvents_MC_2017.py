{
    "RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v1": {
        "TTZZ": [
            "327000"
        ], 
        "TTbarPowheg_Semilept": [
            "346052000"
        ], 
        "ZZZ": [
            "178000"
        ], 
        "WWW": [
            "171000"
        ], 
        "DYjetsM50": [
            "102863931"
        ], 
        "ST_t-channel_T": [
            "129903000"
        ], 
        "TTbarPowheg_Hadronic": [
            "232999999"
        ], 
        "TTWW": [
            "698000"
        ], 
        "WZZ": [
            "298000"
        ], 
        "TTWZ": [
            "350000"
        ], 
        "ST_s_channel": [
            "13620000"
        ], 
        "ZZ": [
            "2706000"
        ], 
        "ST_t-channel_antiT": [
            "69793000"
        ], 
        "WWZ": [
            "178000"
        ], 
        "DYjetsM10to50": [
            "68480179"
        ], 
        "WZ": [
            "7889000"
        ], 
        "TTbarPowheg_Dilepton": [
            "106724000"
        ], 
        "WW": [
            "15634000"
        ]
    }, 
    "RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v2": {
        "TTTT": [
            "10351000"
        ], 
        "ST_tbarW_channel": [
            "5674000"
        ], 
        "TTZ": [
            "31791131"
        ], 
        "W4jets": [
            "9606448"
        ], 
        "TTHToBB": [
            "7825000"
        ], 
        "TTWH": [
            "360000"
        ], 
        "TTZH": [
            "350000"
        ], 
        "TTHToNonBB": [
            "5070989"
        ], 
        "TTHH": [
            "360000"
        ], 
        "TTW": [
            "27662138"
        ], 
        "ST_tW_channel": [
            "5649000"
        ]
    }
}