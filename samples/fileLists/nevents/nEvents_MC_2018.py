{
    "RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1": {
        "TTZZ": [
            "498000"
        ], 
        "TTbarPowheg_Semilept": [
            "476408000"
        ], 
        "ZZZ": [
            "250000"
        ], 
        "WWW": [
            "240000"
        ], 
        "DYjetsM50": [
            "96233328"
        ], 
        "ST_t-channel_T": [
            "178336000"
        ], 
        "TTbarPowheg_Hadronic": [
            "334206000"
        ], 
        "TTWW": [
            "944000"
        ], 
        "WZZ": [
            "300000"
        ], 
        "TTWZ": [
            "498000"
        ], 
        "ST_s_channel": [
            "19365999"
        ], 
        "ZZ": [
            "3526000"
        ], 
        "ST_t-channel_antiT": [
            "95627000"
        ], 
        "WWZ": [
            "248000"
        ], 
        "DYjetsM10to50": [
            "94452816"
        ], 
        "WZ": [
            "7940000"
        ], 
        "TTbarPowheg_Dilepton": [
            "145020000"
        ], 
        "WW": [
            "15679000"
        ]
    }, 
    "RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2": {
        "TTTT": [
            "13058000"
        ], 
        "ST_tbarW_channel": [
            "7749000"
        ], 
        "TTZ": [
            "32793815"
        ], 
        "W4jets": [
            "9130068"
        ], 
        "TTHToBB": [
            "9668000"
        ], 
        "TTWH": [
            "497000"
        ], 
        "TTZH": [
            "500000"
        ], 
        "TTHToNonBB": [
            "7328993"
        ], 
        "TTHH": [
            "500000"
        ], 
        "TTW": [
            "27686862"
        ], 
        "ST_tW_channel": [
            "7956000"
        ]
    }
}