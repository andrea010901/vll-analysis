{
    "RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11-v1": {
        "ST_tW_channel": [
            "2300000"
        ], 
        "TTZZ": [
            "140000"
        ], 
        "TTbarPowheg_Semilept": [
            "132178000"
        ], 
        "ZZZ": [
            "81000"
        ], 
        "WWW": [
            "71000"
        ], 
        "DYjetsM50": [
            "95170542"
        ], 
        "ST_t-channel_T": [
            "55961000"
        ], 
        "TTbarPowheg_Hadronic": [
            "97260000"
        ], 
        "TTWW": [
            "278000"
        ], 
        "WZZ": [
            "160000"
        ], 
        "TTWZ": [
            "140000"
        ], 
        "ST_s_channel": [
            "5518000"
        ], 
        "ZZ": [
            "1282000"
        ], 
        "ST_t-channel_antiT": [
            "31024000"
        ], 
        "WWZ": [
            "81000"
        ], 
        "ST_tbarW_channel": [
            "2300000"
        ], 
        "DYjetsM10to50": [
            "25799525"
        ], 
        "WZ": [
            "7934000"
        ], 
        "TTbarPowheg_Dilepton": [
            "37505000"
        ], 
        "WW": [
            "15859000"
        ]
    }, 
    "RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11-v2": {
        "TTTT": [
            "4207000"
        ], 
        "TTZ": [
            "17127765"
        ], 
        "W4jets": [
            "4598664"
        ], 
        "TTHToBB": [
            "4622000"
        ], 
        "TTWH": [
            "140000"
        ], 
        "TTZH": [
            "140000"
        ], 
        "TTHToNonBB": [
            "1977996"
        ], 
        "TTHH": [
            "140000"
        ], 
        "TTW": [
            "14186416"
        ]
    }
}