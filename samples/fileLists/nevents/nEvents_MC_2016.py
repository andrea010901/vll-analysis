{
    "RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17-v1": {
        "TTZZ": [
            "152000"
        ], 
        "TTbarPowheg_Semilept": [
            "144722000"
        ], 
        "ZZZ": [
            "72000"
        ], 
        "WWW": [
            "69000"
        ], 
        "TTZ": [
            "14329456"
        ], 
        "DYjetsM50": [
            "82448537"
        ], 
        "ST_t-channel_T": [
            "63073000"
        ], 
        "TTbarPowheg_Hadronic": [
            "107067000"
        ], 
        "TTWW": [
            "309000"
        ], 
        "WZZ": [
            "137000"
        ], 
        "TTWZ": [
            "159000"
        ], 
        "ST_s_channel": [
            "5471000"
        ], 
        "ZZ": [
            "1151000"
        ], 
        "ST_t-channel_antiT": [
            "30609000"
        ], 
        "WWZ": [
            "67000"
        ], 
        "TTW": [
            "14396003"
        ], 
        "DYjetsM10to50": [
            "22388550"
        ], 
        "WZ": [
            "7584000"
        ], 
        "TTbarPowheg_Dilepton": [
            "43546000"
        ], 
        "WW": [
            "15821000"
        ]
    }, 
    "RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17-v2": {
        "TTTT": [
            "4544000"
        ], 
        "ST_tbarW_channel": [
            "2554000"
        ], 
        "W4jets": [
            "4816485"
        ], 
        "TTHToBB": [
            "4937000"
        ], 
        "TTWH": [
            "160000"
        ], 
        "TTZH": [
            "160000"
        ], 
        "TTHToNonBB": [
            "2240994"
        ], 
        "TTHH": [
            "152000"
        ], 
        "ST_tW_channel": [
            "2491000"
        ]
    }
}