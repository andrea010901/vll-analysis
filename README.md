# Vector Like Lepton search 
## Analysis Framework using COFFEA

Vector-Like Lepton (VLL) analysis looking into the phase-space of events with 2 leptons plus (0/1/2) taus in the final state. The analysis workflow makes use of the coffea framework. Documentation can be found here :
- [Coffea](https://coffeateam.github.io/coffea/)

# Instructions

**The first time :**
```
sh bootstrap.sh
```

**To produce a list of files to run on querying DAS**

```
voms-proxy-init -voms cms -rfc
python prepareFilelistJSON.py --inputFile inputlist.json --outputFile outputlist.json
```
**inputlist.json** should have the following format :
{
    "Campaign_name":{
    "My name":"Process_name"
   }
}

where dataset=/Process_name/Campaign_name/dataTier

One should properly configure the outputs produced to match the needen input file style for the analysis code. The redirector has to be added before each file in the outputlist.json file i.e. redirector = "root://cms-xrd-global.cern.ch//" or redirector = "root://cmsxrootd.fnal.gov//". Example of the input format can be found in the file *testData.py*. 

**To run interactively :**
```
./shell
python runProcessor.py
```

**For running on condor:**


We can use the **runFullDataset.py** script. First make sure you have a valid grid certificate :
```
export X509_USER_PROXY=${HOME}/.x509up_${UID}
```

Then one can run :
```
./shell
/usr/local/bin/python runFullDataset.py MCTTbar2l -e wiscjq --era "2018" --workers 400
```

Usage of **runFullDataset.py** is described below :
```
runFullDataset.py [-h] [--era {"2018","2017","2016pre","2016post"}] [--chunksize CHUNKSIZE] [--maxchunks MAXCHUNKS] [--workers WORKERS] [--outdir OUTDIR] [--batch] [-e {local,wiscjq,debug}] {MCTTbar1l,MCTTbar2l,TTV,TTVV,MCZJets,DiBoson,TriBoson,MCSingleTop,MCOther,MCAll,Data}

positional arguments:
  {MCTTbar1l,MCTTbar2l,TTV,TTVV,MCZJets,DiBoson,TriBoson,MCSingleTop,MCOther,MCAll,Data}
                        Name of process to run

optional arguments:
  -h, --help            show this help message and exit
  --era {2018,2017,2016pre,2016post}
                        Era to run over
  --chunksize CHUNKSIZE
                        Chunk size
  --maxchunks MAXCHUNKS
                        Max chunks
  --workers WORKERS     Number of workers
  --outdir OUTDIR       Where to put the output files
  --batch               Batch mode (no progress bar)
  -e {local,wiscjq,debug}, --executor {local,wiscjq,debug}
                        How to run the processing

```


**For plotting :**
```
python plotting.py
```
