import numpy as np

import matplotlib.pyplot as plt
from cycler import cycler
from coffea import util
from coffea.processor import accumulate
import hist
import hist.intervals
import uproot
import os

from vll.utils.plotting import plotWithRatio, RebinHist, SetRangeHist, GroupBy
from vll.utils.crossSections import lumis, crossSections, nevents

saveHists = True
plotComp = True

era = "2018"


outdir = "Plots_CR"
if not os.path.exists(outdir):
    os.makedirs(outdir)

# Background MC accumulator 
outputMC = accumulate(
    [
		util.load('test.coffea'),
        util.load('Outputs/output_MCTTbar2l_run20230503_103802.coffea'),
        util.load('Outputs/output_TTV_run20230505_082821.coffea'),
        util.load('Outputs/output_DiBoson_run20230505_084238.coffea'),
		util.load('Outputs/output_TriBoson_run20230505_085809.coffea'),
		util.load('Outputs/output_TTVV_run20230505_173652.coffea'),
    ]
)


# Signal MC accumulator 
outputSignal = accumulate(
    [
		util.load('test.coffea'),
    ]
)

# Data accumulator 
outputData = accumulate(
    [
		util.load('Outputs/output_Data_run20230504_135252.coffea'),
        util.load('Outputs/output_Data_run20230505_065235.coffea'),
    ]
)

#######################
# Grouping
#######################
groupingBackground = {
    "Diboson": ["WW","ZZ","WZ", ],
    "Triboson": ["WWW","ZZZ","WWZ","WZZ",],
    "ttVV": [ "TTHH","TTWW","TTZZ","TTZH","TTWZ","TTWH", ],
    "4top": [ "TTTT", ],
    #"DY": ["DYjetsM50",],
    #"Single-top": [ "ST_tW_channel","ST_tbarW_channel","ST_s_channel","AntiTW_noFH","ST_t-channel_antiT","ST_t-channel_T",],
    "ttH" : [ "TTHToNonBB","TTHToBB",],
    "ttV" : [ "TTW","TTZ",],
    "ttbar": [ "TTbarPowheg_Dilepton",],
}

groupingSignal = {
    "VLL_NN_M-600GeV": ["VLL_NN_M-600GeV",],
    "VLL_EN_M-600GeV": ["VLL_EN_M-600GeV",],
    "VLL_EE_M-600GeV": ["VLL_EE_M-600GeV",],
}

groupingCategory = {
    "CR": ['4j2b','4j3b'],
    "SR": ['4j4b'],
}


groupingTaus = {
    "notau": ['zeroTau'],
    "tau": ['oneTau','twoTau'],
}

# Plot superimposed distributions
variableList = ["MET_pt",'lead_lep_pt','sublead_lep_pt','lep_mll','event_HT','event_bjet_HT','lead_jet_pT','sub_jet_pT','lead_bjet_pT','sub_bjet_pT']
axes = ['dataset', 'lepFlavor','tauMultiplicity','bJetMultiplicity','systematic']
xRange1=[0,300]
xRange2=[0,500]
xRange3=[0,1000]
xRanges = [xRange2,xRange2,xRange1,xRange2,[300,1300],xRange3,xRange3,xRange3,xRange3,xRange3]
labels = ['MET [GeV]','Leading lepton $p_{T}$ [GeV]','Sub-leading lepton $p_{T}$ [GeV]','$m_{ll}$ [GeV]',' Event jet $H_{T}$ [GeV]',' Event b-jet $H_{T}$ [GeV]','Leading b-jet $p_{T}$ [GeV]','Sub-leading jet $p_{T}$ [GeV]','Leading b-jet $p_{T}$ [GeV]','Sub-leading b-jet $p_{T}$ [GeV]']

#######################
# MC histogram grouping
#######################

# Signal
histList = []
for samp, sampList in groupingSignal.items():
    histList += [outputSignal[s] for s in sampList]

outputSigHist = accumulate(histList)
for key, histo in outputSigHist.items():
    if isinstance(histo, hist.Hist):
        outputSigHist[key] = GroupBy(histo, 'dataset', 'dataset', groupingSignal)

# Background
histList = []
for samp, sampList in groupingBackground.items():
    histList += [outputMC[s] for s in sampList]

outputMCHist = accumulate(histList)
for key, histo in outputMCHist.items():
    if isinstance(histo, hist.Hist):
        outputMCHist[key] = GroupBy(histo, 'dataset', 'dataset', groupingBackground)

# Data
outputDataHist = accumulate([histo for key, histo in outputData.items()])


#######################
# Data vs MC histograms
#######################

reg = "CR"

for var, rg, lb in zip(variableList,xRanges,labels):
    
    # Signal
    hs = outputSigHist[var][{'lepFlavor':sum}]
    hs = hs[{'tauMultiplicity':sum}]
    hs = hs[{'systematic':'nominal'}]
    hs = GroupBy(hs, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
    hs = hs[{'bJetMultiplicity':reg}]

    # Background
    h = outputMCHist[var][{'lepFlavor':sum}]
    h = h[{'tauMultiplicity':sum}]
    h = h[{'systematic':'nominal'}]
    h = GroupBy(h, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
    h = h[{'bJetMultiplicity':reg}]

    #if isinstance(h, hist.Hist):
    #   h *= 1./4.

    # Data
    hData = outputDataHist[var][{'lepFlavor':sum,'systematic':sum,'tauMultiplicity':sum,'dataset':sum}]
    hData = GroupBy(hData, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
    hData = hData[{'bJetMultiplicity':reg}]

    # Plot
    luminosity = lumis[era]/1000.
    plotWithRatio(h, hData, overlay='dataset', stacked=True, xRange=rg, yRange=[0.01,None], logY=True, leg='upper right',binwnorm=None, xlabel=lb, lumi=luminosity)
    plt.savefig(outdir+'/'+var+'.png')
    plt.close()


#############################
# Print expected S & B events
#############################


for region in ['4j2b','4j3b','4j4b']:
    print('Region is ', region)
    h = outputMCHist['MET_pt'][{'lepFlavor':sum}]
    h = GroupBy(h,'tauMultiplicity', 'tauMultiplicity',groupingTaus)
    h = h[{'tauMultiplicity':sum}]
    #h = h[{'tauMultiplicity':'tau'}]
    h = h[{'systematic':'nominal'}]
    h = h[{'bJetMultiplicity': region}]

    for samp, sampList in groupingBackground.items():
        print('Number of events for dataset : ', samp, '   is ',  h[{'dataset':samp, 'met':sum}])

    hs = outputSigHist['MET_pt'][{'lepFlavor':sum}]
    hs = GroupBy(hs,'tauMultiplicity', 'tauMultiplicity',groupingTaus)
    hs = hs[{'tauMultiplicity':sum}]
    #hs = hs[{'tauMultiplicity':'tau'}]
    hs = hs[{'systematic':'nominal'}]
    hs = hs[{'bJetMultiplicity':region}]

    for samp, sampList in groupingSignal.items():
        print('Number of events for dataset : ', samp, '   is ',  hs[{'dataset':samp, 'met':sum}])

# For debugging
#fig, ax = plt.subplots(figsize=(10,10))
#outputMCHist['MET_pt'][{'lepFlavor':sum}].plot1d(overlay='systematic', label=['eleEffWeightDown','eleEffWeightUp','nominal','']);
#plt.legend();

#######################
# CutFlow
#######################
hData = outputDataHist['CutFlow'][{'dataset':sum}]
h = outputMCHist['CutFlow']
plotWithRatio(h, hData, overlay='dataset', yRange=[0.01,None], logY=True, leg='upper right',binwnorm=None, xlabel="Cutflow")
plt.savefig(outdir+'/Cutflow.png')

# Check first bin in CutFlow
# outputdebug = accumulate(histList)
#outputdebug = accumulate([histo for key, histo in outputMCHist.items()])
#print(histList)
#h_debug = outputdebug['CutFlow']
#for key, sample in groupingBackground.items():
#    print(key,sample)
#    print(h_debug[{'dataset':sample,'cut':10}])


#######################
# Signal vs Background
#######################

groupingSvsB = {
    "Background ttbar": ["TTbarPowheg_Dilepton"],
    "Signal": ["VLL_NN_M-600GeV","VLL_EN_M-600GeV","VLL_EE_M-600GeV"],
}

outputHist = accumulate([histo for key, histo in outputMC.items()])

for key, histo in outputHist.items():
    if isinstance(histo, hist.Hist):
        outputHist[key] = GroupBy(histo, 'dataset', 'dataset', groupingSvsB)

for var, rg, lb in zip(variableList,xRanges,labels): 

    h = outputHist[var][{'lepFlavor':sum}]
    h = h[{'tauMultiplicity':sum}]
    h = h[{'systematic':'nominal'}]
    h = GroupBy(h, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
    h = h[{'bJetMultiplicity':reg}]

    #if isinstance(h, hist.Hist):
    #   h *= 1./h

    # Plot comparison 
    plotWithRatio(h=h, hData=None, overlay='dataset', stacked=False, density=True, leg='upper right',binwnorm=None, xlabel=lb, lumi=luminosity, xRange=rg)
    plt.savefig(var+'_SvsB.png')  
    plt.close()

'''
# Print out integrals
for group_name,dataset_name in grouping.items():
    for name in dataset_name:
        print(name)
        print("Integral: ", ((h_met.integrate('dataset',name)).sum('met')).values())
        #print("Integral with HT cut: ", ((h_event_bjet_HT.integrate('dataset',name)).sum('event_HT')).values())
        #test = h_event_bjet_HT.integrate('dataset',name)
        #test = test.integrate('event_HT', int_range=slice(300,1000,))
        #print('test ',test.values()) 

outdir = "RootFiles"
if not os.path.exists(outdir):
    os.makedirs(outdir)

if saveHists:
    outputFile = uproot.recreate(os.path.join(outdir, "Output.root"))
    datasets = h_met.axis("dataset").identifiers()
    #categories = h_met.axis("bJetMultiplicity").identifiers()
    #for cat in categories:
    for _dataset in datasets:
        #for var in variableList:
        #outputFile[f"{var}/{_dataset}"] = h_met[{"dataset": slice(_dataset, sum)}]
        #outputFile[f"{var}/{_dataset}"] = histoList[0].integrate("dataset", _dataset).to_hist()
        outputFile[f"{_dataset}_met"] = h_met.integrate("dataset", _dataset).to_hist()
        #outputFile[f"{_dataset}_met"] = h_met.integrate("dataset", _dataset).to_hist()
        #outputFile[f"{_dataset}_lead_lep_pt"] = h_lead_lep_pt.integrate("dataset", _dataset).to_hist()
        #outputFile[f"{_dataset}_sublead_lep_pt"] = h_sublead_lep_pt.integrate("dataset", _dataset).to_hist()
        outputFile[f"{_dataset}_cutflow"] = h_cutflow.integrate("dataset", _dataset).to_hist()

    outputFile.close()
'''
